CURRENTDIR=`dirname "$0"`
echo "Current dir: `dirname "$0"`"
while read -r line;
do
    patchFile=$(echo $line | cut -d ':' -f 1)
    directory=$(echo $line | cut -d ':' -f 2)
    cd ${ANDROID_BUILD_TOP}/$directory
    git apply ${ANDROID_BUILD_TOP}/patches/patches/$patchFile
    cd ${ANDROID_BUILD_TOP}
done < $CURRENTDIR/relations
                    